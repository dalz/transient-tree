#include <stdatomic.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "tree.h"

struct tree {
	atomic_int rc;
	bool is_const;

	int key;

	/* the const qualifier could be removed, to allow mutation
	 * (but *only if [is_const] is false*) - this mostly means that
	 * [*value] may be mutated only through tree functions (which is not
	 * especially useful in this case) */
	char const *value;

	struct tree *left, *right;
};

/* Creates a new mutable node with reference count 1.
 *
 * Returns 0 if allocation fails.*/
struct tree *
tree_new(int k, char const *v, struct tree *l, struct tree *r)
{
	struct tree *t = malloc(sizeof(*t));
	if (!t) return 0;

	*t = (struct tree){
		.rc = 1,
		.key = k,
		.value = v,
		.left = l,
		.right = r
	};

	return t;
}

/* Creates a mutable shallow copy of [t], with reference count 1,
 * and increases [t]'s children's reference counts.
 * Takes care of propagating [t->is_const] if necessary.
 *
 * Returns 0 if allocation fails. */
struct tree *
tree_clone(struct tree *t)
{
	struct tree *res = malloc(sizeof(*res));
	if (!res) return 0;

	memcpy(res, t, sizeof(*t));
	res->rc = 1;
	res->is_const = false;

	if (res->left) tree_hold(res->left);
	if (res->right) tree_hold(res->right);

	if (t->is_const) {
		if (t->left)
			t->left->is_const = true;
		if (t->right)
			t->right->is_const = true;
	}

	return res;
}

/* Increases [t]'s reference count.
 * If at the time of calling this [t->rc] is 0, the tree is being freed and
 * holding it is not possible. A serious program should try to handle this
 * gracefully, but here we'll just exit. */
void
tree_hold(struct tree *t)
{
	if (atomic_fetch_add(&t->rc, 1) == 0)
		exit(1);
}

/* Decrements [t]'s reference count, and frees the node if it reaches 0. */
void
tree_free(struct tree *t)
{
	if (atomic_fetch_sub(&t->rc, 1) != 1)
		return;

	if (t->left)
		tree_free(t->left);
	if (t->right)
		tree_free(t->right);

	free(t);
}

/* Makes [t] persistent. Future operations on [t] will be copy-on-write.
 * Additionaly, increases [t]'s reference count. */
void
tree_persist(struct tree *t)
{
	t->is_const = true;
	tree_hold(t);
}

/* Returns the value at key [k], or 0 if it's not in the tree. */
char const *
tree_get(struct tree *t, int k)
{
	while (t) {
		if (t->key == k)
			return t->value;

		t = k > t->key ? t->right : t->left;
	}

	return 0;
}

/* Inserts (k, v) into the tree, cloning nodes that are immutable.
 * Returns the new tree, which will be [t] if [t->is_const] is false.
 * If it isn't, [tree_free] is called on [t], so [tree_hold] it
 * (implicitly done by [tree_persist]) if necessary.
 *
 * Returns 0 if allocation fails. */
struct tree *
tree_put(struct tree *t, int k, char const *v)
{
	struct tree *root = t;
	struct tree **p = &root;

	while (t) {
		if (t->is_const) {
			t = tree_clone(t);
			if (!t) return 0;

			tree_free(*p);
			*p = t;
		}

		if (t->key == k) {
			t->value = v;
			return root;
		}

		p = k > t->key ? &t->right : &t->left;
		t = *p;
	}

	*p = tree_new(k, v, 0, 0);
	if (!*p) return 0;

	return root;
}

/* Functions for printing the tree in graphviz format. */

static void
tree_graphviz_rec(struct tree *p, struct tree *t)
{
	if (!t) return;

	printf("p%p [label=\"%d\\n%s\"];", (void *)t, t->key, t->value);

	if (p)
		printf("p%p -> p%p;", (void *)p, (void *)t);

	tree_graphviz_rec(t, t->left);
	tree_graphviz_rec(t, t->right);
}

void
tree_graphviz(struct tree *t)
{
	tree_graphviz_rec(0, t);
}

void
tree_graphviz_start(char const *title)
{
	printf("digraph {");
	printf("edge [arrowhead=none];");
	printf("label=\"%s\";", title);
}

void
tree_graphviz_end(void)
{
	puts("}");
}
