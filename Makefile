.POSIX:

CC = gcc
CFLAGS = -std=c18 -Wall -Wextra -Wpedantic -g -fsanitize=address,undefined

svgs: transient-tree
	./transient-tree | ./generate-svgs.sh

transient-tree: main.c tree.c tree.h
	$(CC) $(CFLAGS) -o $@ main.c tree.c

run: transient-tree
	./transient-tree
