#!/bin/sh
set -e

mkdir -p svgs

n=0
while read -r graph; do
    f="$n-$(printf '%s' "$graph" | sed -E 's/^[^"]*"([^"]*).*/\1/' | tr ' ' - | tr -d '()').svg"
    echo "$graph" | dot -Tsvg -o "svgs/$f"
    n=$((n + 1))
done
