#ifndef TREE_H
#define TREE_H

struct tree;

struct tree *tree_new(int k, char const *v, struct tree *l, struct tree *r);
void tree_hold(struct tree *t);
void tree_free(struct tree *t);
void tree_persist(struct tree *t);
char const *tree_get(struct tree *t, int k);
struct tree *tree_put(struct tree *t, int k, char const *v);

void tree_graphviz(struct tree *t);
void tree_graphviz_start(char const *title);
void tree_graphviz_end(void);

#endif
