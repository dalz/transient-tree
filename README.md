# Transient tree

Code the article "Transient trees in C"
([web en](https://alsd.eu/en/2021-09-03-transient-trees-in-c.html) / 
[web it](https://alsd.eu/it/2021-09-03-alberi-transienti-in-c.html) / 
[gemini en](gemini://alsd.eu/en/2021-09-03-transient-trees-in-c.gmi) / 
[gemini it](gemini://alsd.eu/it/2021-09-03-alberi-transienti-in-c.gmi))
is based on.

Generated visualizations of the trees can be found in the `svgs` directory.
