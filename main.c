#include <assert.h>
#include <stdio.h>

#include "tree.h"

int
main(void)
{
	struct tree *t = tree_new(0, "root", 0, 0);
	struct tree *u;

	/* we should be checking that [t] is not null */
	t = tree_put(t, -10, "foo");
	t = tree_put(t, -5, "bar");
	t = tree_put(t, 10, "baz");
	t = tree_put(t, 5, "fizz");
	t = tree_put(t, 15, "buzz");
	t = tree_put(t, 7, "pippo");

	tree_graphviz_start("Initial tree");
	tree_graphviz(t);
	tree_graphviz_end();

	u = tree_put(t, 0, "tree");
	assert(t == u);

	tree_graphviz_start("Modified root");
	tree_graphviz(t);
	tree_graphviz_end();

	tree_persist(t);
	u = tree_put(t, 6, "pluto");

	tree_graphviz_start("Added node (after taking snapshot)");
	tree_graphviz(u);
	tree_graphviz_end();

	tree_graphviz_start("Snapshot");
	tree_graphviz(t);
	tree_graphviz_end();

	tree_graphviz_start("Structural sharing after one edit");
	tree_graphviz(t);
	tree_graphviz(u);
	tree_graphviz_end();

	u = tree_put(u, -5, "car");
	tree_graphviz_start("Structural sharing after another edit");
	tree_graphviz(t);
	tree_graphviz(u);
	tree_graphviz_end();

	tree_free(t);

	tree_graphviz_start("After freeing the snapshot");
	tree_graphviz(u);
	tree_graphviz_end();

	tree_free(u);
}
